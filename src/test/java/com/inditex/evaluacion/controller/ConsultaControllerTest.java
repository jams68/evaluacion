package com.inditex.evaluacion.controller;

import com.inditex.evaluacion.dto.Response;
import com.inditex.evaluacion.service.PriceService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(ConsultaController.class)
class ConsultaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    PriceService priceService;

    @Test
    public void obtenerProductoTest1() throws Exception{
        Response response = new Response();
        response.setPrecioFinal(35.5);
        response.setCodigoProducto(35455);
        response.setCodigoCadena(1);
        response.setFechaAplicacion("2020-06-14 10:00:00");

        given(priceService.obtenerProducto(any(),anyInt(),anyInt())).willReturn(response);

        mockMvc.perform(get("/tienda/producto/2020-06-14 10:00:00/1/354556")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.codigoProducto").value("35455"))
                .andExpect(jsonPath("$.codigoCadena").value("1"))
                .andExpect(jsonPath("$.precioFinal").value("35.5"))
                .andExpect(jsonPath("$.fechaAplicacion").value("2020-06-14 10:00:00"));

    }

    @Test
    public void obtenerProductoTest2() throws Exception{
        Response response = new Response();
        response.setPrecioFinal(25.5);
        response.setCodigoProducto(35455);
        response.setCodigoCadena(1);
        response.setFechaAplicacion("2020-06-14 16:00:00");

        given(priceService.obtenerProducto(any(),anyInt(),anyInt())).willReturn(response);

        mockMvc.perform(get("/tienda/producto/2020-06-14 16:00:00/1/354556")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.codigoProducto").value("35455"))
                .andExpect(jsonPath("$.codigoCadena").value("1"))
                .andExpect(jsonPath("$.precioFinal").value("25.5"))
                .andExpect(jsonPath("$.fechaAplicacion").value("2020-06-14 16:00:00"));

    }

    @Test
    public void obtenerProductoTest3() throws Exception{
        Response response = new Response();
        response.setPrecioFinal(35.5);
        response.setCodigoProducto(35455);
        response.setCodigoCadena(1);
        response.setFechaAplicacion("2020-06-14 21:00:00");

        given(priceService.obtenerProducto(any(),anyInt(),anyInt())).willReturn(response);

        mockMvc.perform(get("/tienda/producto/2020-06-14 21:00:00/1/354556")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.codigoProducto").value("35455"))
                .andExpect(jsonPath("$.codigoCadena").value("1"))
                .andExpect(jsonPath("$.precioFinal").value("35.5"))
                .andExpect(jsonPath("$.fechaAplicacion").value("2020-06-14 21:00:00"));

    }

    @Test
    public void obtenerProductoTest4() throws Exception{
        Response response = new Response();
        response.setPrecioFinal(30.5);
        response.setCodigoProducto(35455);
        response.setCodigoCadena(1);
        response.setFechaAplicacion("2020-06-15 10:00:00");

        given(priceService.obtenerProducto(any(),anyInt(),anyInt())).willReturn(response);

        mockMvc.perform(get("/tienda/producto/2020-06-15 10:00:00/1/354556")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.codigoProducto").value("35455"))
                .andExpect(jsonPath("$.codigoCadena").value("1"))
                .andExpect(jsonPath("$.precioFinal").value("30.5"))
                .andExpect(jsonPath("$.fechaAplicacion").value("2020-06-15 10:00:00"));

    }

    @Test
    public void obtenerProductoTest5() throws Exception{
        Response response = new Response();
        response.setPrecioFinal(38.95);
        response.setCodigoProducto(35455);
        response.setCodigoCadena(1);
        response.setFechaAplicacion("2020-06-16 21:00:00");

        given(priceService.obtenerProducto(any(),anyInt(),anyInt())).willReturn(response);

        mockMvc.perform(get("/tienda/producto/2020-06-16 21:00:00/1/354556")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.codigoProducto").value("35455"))
                .andExpect(jsonPath("$.codigoCadena").value("1"))
                .andExpect(jsonPath("$.precioFinal").value("38.95"))
                .andExpect(jsonPath("$.fechaAplicacion").value("2020-06-16 21:00:00"));

    }
}