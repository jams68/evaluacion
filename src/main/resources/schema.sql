create  table IF NOT EXISTS `PRICES` (
    ID integer auto_increment primary key,
    BRAND_ID integer,
    START_DATE TIMESTAMP,
    END_DATE TIMESTAMP,
    PRICE_LIST integer,
    PRODUCT_ID integer,
    PRIORITY integer,
    PRICE double,
    CURR varchar(3)
);




