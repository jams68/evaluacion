package com.inditex.evaluacion.repository;

import com.inditex.evaluacion.entity.Prices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PricesRepository  extends JpaRepository<Prices, Integer> {

    @Query(value = "SELECT  * " +
            "from PRICES WHERE START_DATE  <= ?1 AND END_DATE >=?1 AND BRAND_ID = ?2 AND PRODUCT_ID = ?3 ORDER BY PRIORITY DESC",nativeQuery = true)
    List<Prices> findByFecha(String fecha, int productoId, int cadenaId);
}
