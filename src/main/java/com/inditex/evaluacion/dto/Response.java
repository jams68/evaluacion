package com.inditex.evaluacion.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Data
public class Response {
    private int codigoProducto;
    private int codigoCadena;
    private String fechaAplicacion;
    private Double precioFinal;

}
