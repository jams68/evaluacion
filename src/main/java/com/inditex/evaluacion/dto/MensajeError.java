package com.inditex.evaluacion.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MensajeError {
    private String mensaje;
}
