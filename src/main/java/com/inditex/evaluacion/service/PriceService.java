package com.inditex.evaluacion.service;

import com.inditex.evaluacion.dto.Response;
import com.inditex.evaluacion.entity.Prices;
import com.inditex.evaluacion.repository.PricesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Service
public class PriceService {
    @Autowired
    PricesRepository pricesRepository;

    public Response obtenerProducto(String fecha, int productoId, int cadenaId) throws ParseException {
        Response response = new Response();
        Prices price;
       List<Prices> lista = pricesRepository.findByFecha(fecha,productoId,cadenaId);

       if (lista.isEmpty()){
           return null;
       }else {
           response.setCodigoProducto(lista.get(0).getProductId());
           response.setCodigoCadena(lista.get(0).getBrandId());
           response.setFechaAplicacion(fecha);
           response.setPrecioFinal(lista.get(0).getPrice());
           return response;
       }




    }
}
