package com.inditex.evaluacion.controller;

import com.inditex.evaluacion.dto.Response;
import com.inditex.evaluacion.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@RestController
@RequestMapping("/tienda")
public class ConsultaController {

    @Autowired
    PriceService priceService;

    @GetMapping("/producto/{fecha}/{productoId}/{cadenaId}")
    public ResponseEntity obtener(@PathVariable("fecha") String fecha, @PathVariable("productoId") String productoId, @PathVariable("cadenaId") String cadenaId ) throws ParseException {

        Response response =  priceService.obtenerProducto(fecha,Integer.parseInt(productoId),Integer.parseInt(cadenaId));

        if(response == null ){
            return  ResponseEntity.notFound().build();
        }else {
            return ResponseEntity.ok(response);
        }

    }
}
